# InfluxDB/Event Store/SQL Server - A simple benchmark
InfluxData is an alerting, visualization and backend platform for building custom monitoring solutions for servers, sensors, storage appliances, network infrastructure, apps, logs and more. One of the core components is InfluxDB. InfluxDB is an open source database created to specifically handle time series data with high availability and high performance requirements.

To learn more about InfluxData, visit https://influxdata.com

Event Store is a database for supporting the concept of Event Sourcing. Event Store stores data as a series of immutable events over time, making it easy to build event-sourced applications.

To learn more about Event Store, visit https://geteventstore.com

The objective of this post is to compare the time it takes to write "events" to InfluxDB, EventStore and SQLServer using HTTP APIs supplied by each platform.

## Test Environment
* The tests were carried out on a MAC OS X (Yosemite) 2.3 GHz Intel i7 with 16Gig RAM
* The SQL Server was running on VMWare Fusion on the MAC
* CURL was used to communicate with Event Store, InfluxDB and SQLServer (via a middleware HTTP service)
* To compensate for the SQLServer running on a VM, a "no-op" SQL query was run on the SQL Server
* InfluxDB and Event Store are run "out of the box", no tuning was performed

## This post describes how to
* Download and setup InfluxDB on MAC OSX
* Download and setup Event Store on MAC OSX
* Run a simple bash script to write "events" to InfluxDB and Event Store using the supplied HTTP APIs
* Compare the time it takes to write the "events" to each platform
* As the REST service front-ending the SQLServer is proprietary, only the SQLServer results are provided

## Download and setup InfluxDB
Installing and starting InfluxDB is quite simple. There is also a CLI to manage databases and perform CRUD operations - see https://docs.influxdata.com/influxdb/v0.10/introduction/getting_started
* Installation
```influx-install
$ brew update
$ brew install influxdb
To start the Server, open a console terminal and type
$ influxd -config /usr/local/etc/influxdb.conf
```
* Create an Influx DB
```influx-db
open a console terminal
$ influx
Connected to http://localhost:8086 version 0.10.0 InfluxDB shell 0.10.0 >
> CREATE DATABASE perftest
```

## Download and setup Event Store
Installing and starting Event Store is also simple. There is no DB to connect to but there is a Dashboard to view and manage the store at http://127.0.0.1:2113/web/index.html#/
* Installation
```evenstore-install
Go to https://geteventstore.com/downloads and select "Get EventStore for OSX"
$ gunzip EventStore-OSS-MacOSX-*.tar.gz
$ tar -xvf EventStore-OSS-MacOSX-*.tar to your favourite directory
$ cd EventStore-OSS-MacOSX-*
To start the Server type
$ ./run-node.sh
```
* The Event Store
The Event Store operates on the concept of Event Streams. These are partition points in the system. There is no DB to setup.

## Run the InfluxDB Writer Bash Script
If you havent already done so, clone the code from ```git```.
```checkout
git clone https://bitbucket.org/jbyrneie/influxDB-eventStore
```

Run the bash script. The script uses ```curl``` and the InfluxData HTTP API to write the number of "events" specified in the input parameter to the InfluxDB ```perftest``` stream and logs the time it took to write the events. Each event is written sequentially and no batching is performed.

```run
./influx-db-writer <#numEvents>
```

## Viewing Events in the Influx DB
Managing/viewing data in InfluxDB is very similar to SQLServer. Run the ```influx``` command, here are some examples

```run
$ influx
Connected to http://localhost:8086 version 0.10.1
InfluxDB shell 0.10.1
> show databases;
> use test;
```

## Run the Event Store Writer Bash Script
Run the ```event-store-writer```bash script. The script uses ```curl``` and the Event Store HTTP API to write the number of "events" specified in the input parameter to the Event Store "test" stream and logs the time it took to write the events. Each event is written sequentially and no batching is performed.

```run
./event-store-writer <#numEvents>
```

## Viewing Events in the Event Store
You can view the events in the Event Store by navigating to Event Store dashboard at http://127.0.0.1:2113/web/index.html#/

```view
userName: admin
password: changeit
```

When logged in, click the ```Stream Browser``` tab

![alt text](https://bytebucket.org/jbyrneie/influxdb-eventstore/raw/bba8126f6a0de54dbf1c04005ea0ef0a905db77e/docs/images/eventStoreDashboard.png "Dashboard")

## Benchmark Results
| Platform | Number of Events | Time Taken (Seconds)
|----------|------------------|--------------------|
|    Event-Store     |      1000         |    6
|    InfluxDB     |      1000         |    10
|    SQLServer     |      1000         |    29
|    Event-Store     |      10K          |    73
|    InfluxDB    |      10K          |    100
|    SQLServer    |      10K          |    280
|    Event-Store     |      100K         |    957
|    InfluxDB     |      100K         |    1062


## Conclusions
While none of the environments (Event Store, InfluxDB, SQLServer) were optimized, Event Store performs the best, followed by InfluxDB and SQLServer lagging behind.

Coupled with tuning, clustering and load balancing both Event Store and InfluxDB are excellent platforms for high scalability event driven environments.
